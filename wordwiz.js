/*
Author: Jordan Blake 2016
-search module: works with a modified wordnet lexicon in sqlite
*/
module.exports = {

          jsHelper: require(__dirname + "/jsHelper.js"),
          
          semlinks:require(__dirname + "/semlinks.js"),
          
          /********
           * isNonEmptyArray:(): takes array, returns boolean           *
           ********/
          
          isNonEmptyArray:function(object)
          {
                    
            return object instanceof Array && object.length > 0;        
                    
          },
          
          
          sqliteToInMemDb:function(dexiconData, tableNames)
          {
                    
                var inMem = require(__dirname + '/inMem.js');
                    
                  inMem.init('dexicon2', dexiconData, tableNames);
                  
                  return inMem;
                    
                    
          },
          
          
          
            /********
           * Word:(): takes string, object, returns object
           * -purpose: initialize a word from the lexicon, with functions to complete related data
           *
           ********/

          Word: function(titleTag, mainWordObj) {

                    return {
                              
                              titleTag:titleTag,
                           
                              lemma:mainWordObj.lemma,
                              
                              pos:mainWordObj.pos,
                              
                              lexdomainname :mainWordObj.lexdomainname,
                              
                              definition:mainWordObj.definition,
                              
                              synonyms:mainWordObj.directSynonyms,
                              
                              antonyms:mainWordObj.antonyms,
                              
                              casedLemma:mainWordObj.casedLemma,
                              
                              derivations:mainWordObj.derivations,
                                 
                              synsetid:mainWordObj.synsetid,
                              

                            //  word: mainWordObj,
                            
                            completionObject:{},

                              semlinks: {},

                              statistics: {},

                              samples: {},

                              getSemlinks: function(self, options, callback) {
                                      
                                      var linkTypes = ['hypernym', 'hyponym', 'instance hypernym', 'substance meronym', 'part holonym', 'part meronym', 'member meronym', 'instance hyponym', 'member holonym', 'substance holonym'];
 
                                       self.semlinks = module.exports.appendSemlinksBySynsetId(module.exports.WordWizard.database, options, this.synsetid, linkTypes, callback);
                                        
                                        
                                        options.recursionLevel = options.recursionLevel >= 0 ? 1 : options.recursionLevel; 
                                   
                                        
                                        
                              },
                              
                              getSemlinksRecursive:function(self, options, callback)
                              { 
                                 
                                 
                                 options.recursiveDepth = options.recursiveDepth >= 0 ? options.recursiveDepth : 1 ;
                                 
                                       
                                      //  getSemlinksByType:function(sqliteDb, options, synset1id, linkType, myArray, recursiveDepth, callback)
                                self.semlinks = module.exports.semlinks.getSemlinksByType(module.exports.WordWizard.database, options, this.synsetid, options.recursiveLinkType, [], options.recursiveDepth, callback);
                                       
                                       
                              },

                              getStatistics: function(self, options, callback) {
                                                  
                                              
                                         
                                        self.statistics = module.exports.appendStatisticsByLemmaAndPos(module.exports.WordWizard.database, options, this.lemma, this.pos, callback);

                              },

                              getSamples: function(self, options, callback) {
                                        
                                       
                                      
                                        self.samples = module.exports.appendSamplesBySynsetId(module.exports.WordWizard.database, options, this.synsetid, callback);

                              }

                    }

          },
          
          
          
            /********
           * WordWizard:(): module object for word data retrieval
           * -purpose: apply functionality and gather data for an [] of words
           *
           ********/

          WordWizard: {

                    database: false, //reference to the 'dexicon' database

                    callback: function() {},

                    init: function(database) {

                              this.database = database;

                    },
                    
                       
            /********
           * getWords:(): takes searchWordsArray: (['apple', 'lizard', 'beer']) || false , searchSynsetsArray
           * -purpose: apply functionality and gather data for an [] of words
           *
           ********/

                    getWords: function(searchWordsArray, searchSynsetsArray, callback) {

                              var valid = module.exports.isNonEmptyArray(searchWordsArray)  || module.exports.isNonEmptyArray(searchSynsetsArray) ? true : false;

                              if (valid && this.database) {

                                        this.searchWordsArray = searchWordsArray;
                                        
                                        this.searchSynsetsArray = searchSynsetsArray;

                                        this.callback = callback;
                                        
                                        
                                        if (this.searchSynsetsArray instanceof Array) {
                                                  module.exports.searchWordsArrayBySynsetId(this.database, this.searchSynsetsArray, this.callback);

                                        var wordData;
                                        return wordData;

                                        }
                                       
                                        module.exports.searchWordsArrayByLemma(this.database, this.searchWordsArray, this.callback);

                                        var wordData;
                                        return wordData;

                                       
                                       
                              }

                              return [];

                    }

          },
          
          
             /********
           * searchWordsArrayBySynsetId:(): takes database, synsetList [], callback(err, dataOut);
           * -purpose: get specific words from the data, using the synsetids of desired words
           *
           ********/
          
             searchWordsArrayBySynsetId: function(dexiconData, synsetList, callback) {

                    var fullWordStructure = [];
                    
                    console.log("searchWordsArrayBySynsetId():processing:" + JSON.stringify(synsetList));

                    var synsetInStr = module.exports.getQueryInStr(synsetList);

                    var wordResults = [];

                    var query = "SELECT * FROM wordsspecial WHERE synsetid COLLATE NOCASE IN" + synsetInStr + " LIMIT 0, 50 ; ";

                    dexiconData.all(query, function(err, rows) {

                              if (err) {

                                      return  console.log(err);

                              }


                              if (!rows || rows.length < 1) {

                                        if (callback && typeof(callback) === 'function') {
                                                return  callback(false, []);
                                        }

                              }

                              console.log("searchWordsArrayByLemma:(): Processing Query:");

                              for (var x = 0; x < rows.length; x++) {

                                        console.log("searchWordsArrayByLemma:():Query Result brought rows of length:" + rows.length);

                                        var mainWord = rows[x];
                                        
                                        fullWordStructure.push(module.exports.Word(mainWord.lemma, mainWord));
                                
                                        if (x === rows.length - 1) {

                                                 

                                        }


                              }
                              
                               callback(false, fullWordStructure);


                    });

                    return fullWordStructure;


          },

              /********
           * searchWordsArrayByLemma:(): takes database, lemmaList [], callback(err, dataOut);
           * -purpose: get specific words from the data, using the lemma of the words, such as ['cat', 'dog', 'mouse']
           *
           ********/

          searchWordsArrayByLemma: function(dexiconData, lemmaList, callback) {

                    var fullWordStructure = [];
                    
                    console.log("Lemma List:" + lemmaList);

                    var lemmaInStr = module.exports.getQueryInStr(lemmaList);

                    var wordResults = [];

                    var query = "SELECT * FROM wordsspecial WHERE lemma COLLATE NOCASE IN" + lemmaInStr + " LIMIT 0, 50 ; ";

                    dexiconData.all(query, function(err, rows) {

                              if (err) {

                                      return  console.log(err);

                              }


                              if (!rows || rows.length < 1) {

                                        if (callback && typeof(callback) === 'function') {
                                                return  callback(false, []);
                                        }

                              }

                              console.log("searchWordsArrayByLemma:(): processing query result:");

                              for (var x = 0; x < rows.length; x++) {

                                        console.log("searchWordsArrayByLemma:():Returned Rows has length:" + rows.length);

                                        var mainWord = rows[x];
                                        
                                        fullWordStructure.push(module.exports.Word(mainWord.lemma, mainWord));
                                
                                      

                              }
                              
                               callback(false, fullWordStructure);


                    });

                    return fullWordStructure;


          },

          
           /********
           * appendSemlinksBySynsetId:(): takes database, options, synsetid, linkTypes [], completeCallback(err, dataOut)
           * -purpose: append linkedWords to the object of existing words
           *
           ********/
          

          appendSemlinksBySynsetId: function(dexiconData,options, synsetid, linkTypes, completeCallback) {

                    var linkTypeInStr = module.exports.getQueryInStr(linkTypes);


                    var query = " SELECT DISTINCT * FROM semlinksspecial WHERE   semlinksspecial.synset1id='" + synsetid + "' AND semlinksspecial.link  COLLATE NOCASE IN" + linkTypeInStr + "     LIMIT  0, 30 ; ";


                    var linkedWords = [];


                    dexiconData.all(query, function(err, rows) {

                              if (err) {

                                        if (completeCallback && typeof(completeCallback) === 'function') {
                                                  completeCallback(err, linkedWords);
                                        }

                              } else {

                                        for (var x = 0; x < rows.length; x++) {

                                                  console.log("appendSemlinksBySynsetId: got "+rows.length +" linked words");

                                                  var word = rows[x];

                                                  linkedWords[x] = word;


                                                  if (x >= rows.length - 1 && completeCallback && typeof(completeCallback) === 'function') {
                                                       
                                                  }


                                        }
                                        
                                         options.semlinks = false;
                                        
                                           completeCallback(false, linkedWords);
                                       
                              }

                    });



                    return linkedWords;

          },
          
          
              /********
           * appendSamplesBySynsetId:(): takes database, options, synsetid,  completeCallback(err, dataOut)
           * -purpose: append samples to the object of existing words
           *
           ********/
          
          

          appendSamplesBySynsetId:function(dexiconData, options, synsetid, completeCallback) {


                    var query = " SELECT DISTINCT * FROM samplesspecial WHERE   samplesspecial.synsetid='" + synsetid + "'  LIMIT  0, 30 ; ";


                    var linkedWords =  [];


                    dexiconData.all(query, function(err, rows) {

                              if (err) {

                                        if (completeCallback && typeof(completeCallback) === 'function') {
                                                  completeCallback(err, linkedWords);
                                        }


                              } else {

                                        for (var x = 0; x < rows.length; x++) {

                                                  console.log("appendSamplesBySynsetId:got "+rows.length +" rows");


                                                  var word = rows[x];


                                                  linkedWords[x]=word;

                                        }
                                        
                                        options.samples = false;
                                        
                                         completeCallback(false, linkedWords);
                                        
                              }

                    });

                    return linkedWords;

          },



              /********
           * appendStatisticsBySynsetId:(): takes database, options, lemma (str), pos(str), callback(err, dataOut);
           * -purpose: append statistics to the object of existing words
           ********/

          appendStatisticsByLemmaAndPos: function(dexiconData, options, lemma, pos, completeCallback) {

                    var query = " SELECT DISTINCT * FROM  statisticsspecial WHERE  statisticsspecial.lemma='" + lemma + "' AND statisticsspecial.pos='" + pos + "'   LIMIT  0, 30 ; ";


                    var linkedWords = [];


                    dexiconData.all(query, function(err, rows) {

                              if (err) {

                                        if (completeCallback && typeof(completeCallback) === 'function') {
                                                  completeCallback(err, linkedWords);
                                        }


                              } else {

                                        for (var x = 0; x < rows.length; x++) {

                                                  console.log("appendStatisticsByLemmaAndPos:got "+rows.length+" rows" );


                                                  var word = rows[x];

                                                  linkedWords[x]=word;

                                                  if (x >= rows.length - 1 && completeCallback && typeof(completeCallback) === 'function') {
                                                       
                                                  }


                                        }
                                        
                                        options.statistics = false;
                                        
                                          completeCallback(false, linkedWords);  
                                        
                                         
                              }

                    });



                    return linkedWords;

          },

          
          /********
           * getQueryInStr:(): takes str, listIn
           * -purpose: produce string with all array members such as '("cat", "dog", "lizard")' : for building sqlite queries with IN('value1', 'value2')
           ********/


          getQueryInStr: function(listIn) {


          console.log("getQueryInStr():" + listIn);

                    var strOut = "(";

                    for (var x = 0; x < listIn.length; x++) {

                              console.log("getQueryInStr():" + listIn[x]);


                              if (x > 0) {

                                        strOut += ",";
                              }


                              strOut += "'" + listIn[x].trim() + "' ";


                    }

                    strOut += ")";

                    return strOut;

          }


}