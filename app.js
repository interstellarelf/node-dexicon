/* Copyright Jordan Blake - All Rights Reserved
 *
 */



/*
 Express and http modules:
 */



module.exports = function(port)
{



var express = require('express');

var app = express();

var server = require('http').Server(app);


// Allow some files to be server over HTTP
app.use(express.static(__dirname + '/public'));


port = isNaN(port) ? 80 : port;


/*
 SQlite and FS: for data and files:
 */

var sqlite3 = require("sqlite3").verbose();

var fs = require("fs");

var file = "wordwiz.db";
var exists = fs.existsSync(file);

var dexiconData = new sqlite3.Database(file);

var dexicon = require(__dirname + '/wordwiz.js');

dexicon.WordWizard.init(dexiconData);


var inMemData = {

    db: false

}


var lexApi = function () {

    return {

        inMem: false,

        init: function (db) {

            this.inMem = db;

        },


        isParamsValid: function (params) {

            for (var x in params) {

                var allowed = x === 'word-data' || x === 'statistics-data' || x === 'semlinks-data' || x === 'wordnet-views' ? true : false;

                if (!allowed) {
                    return false;
                }


            }


        },


        response: function (params) {

            var x = params['apiSection'];


            var allowed = x === 'word-data' || x === 'statistics-data' || x === 'semlinks-data' || x === 'wordnet-views' ? true : false;

            if (!allowed) {
                return "INVALID PARAMETERS: bottom level of URL must be : 'word-data' || 'statistics-data' || 'semlinks-data' || 'wordnet-views' ";
            }


            if (x !== 'word-data') {

                return myCode();  // process according to anything but full word-data , return result

            }


            if (params[synsetids]) {

                var synsetIdList = synsetids.split(','); //process only according to synsetids

            }

            if (params[views]) {

                var synsetIdList = synsetids.split(','); //process only according to views

            }


        }


    }


    return {


        wordDataSets: [],

        semlinks: [],

        samples: [],

        statistics: []


    }


}


var HandlebarsAndExpressServerControls = {

    init: function () {

        hbs.registerHelper('ifCond', function (v1, v2, options) {
            if (v1 === v2) {
                return options.fn(this);
            }
            return options.inverse(this);
        });


    }
}


var expressAndHTTPServerControls = {

    httpGetInitMain: function () {

        app.use(express.static('examples'));


        app.get('/', function (req, res) {

            res.sendFile(__dirname + '/examples/word-search.html');

        });


            app.get('/dexicon/api/basic', function (req, res) {

                console.log("recieved request");

                var words = req.query && req.query.words ? req.query.words.split(',') : false,

                    synsetids = req.query && req.query.synsetids ? req.query.synsetids.split(',') : false,


                    resXml = req.query && req.query.restype && req.query.restype.toLowerCase() === 'xml' ? true : false;

                var options = {fullData: false, statistics: true, semlinks: true, samples: true, recursive: false};

                getFullInMemWordData(words, synsetids, options, res, resXml); res.end('TODO : rig response');

            });




        app.get('/dexicon/api/samples?', function (req, res) {

            console.log("recieved request");

            var words = req.query && req.query.words ? req.query.words.split(',') : false,

                synsetids = req.query && req.query.synsetids ? req.query.synsetids.split(',') : false,


                resXml = req.query && req.query.restype && req.query.restype.toLowerCase() === 'xml' ? true : false;

            var options = {fullData: false, statistics: false, semlinks: false, samples: true, recursive: false};

            getFullInMemWordData(words, synsetids, options, res, resXml);

        });






        app.get('/dexicon/api/semlinks?', function (req, res) {

            console.log("recieved request");

            var words = req.query && req.query.words ? req.query.words.split(',') : false,

                synsetids = req.query && req.query.synsetids ? req.query.synsetids.split(',') : false,


                resXml = req.query && req.query.restype && req.query.restype.toLowerCase() === 'xml' ? true : false;

            var options = {fullData: false, statistics: false, semlinks: true, samples: false, recursive: true};

            getFullInMemWordData(words, synsetids, options, res, resXml);

        });




        app.get('/dexicon/api/statistics?', function (req, res) {

            console.log("recieved request");

            var words = req.query && req.query.words ? req.query.words.split(',') : false,

                synsetids = req.query && req.query.synsetids ? req.query.synsetids.split(',') : false,


                resXml = req.query && req.query.restype && req.query.restype.toLowerCase() === 'xml' ? true : false;

            var options = {fullData: false, statistics: true, semlinks: false, samples: false, recursive: false};

            getFullInMemWordData(words, synsetids, options, res, resXml);

        });


            app.get('/dexicon/api/deep?', function (req, res) {

                console.log("recieved request");

                var words = req.query && req.query.words ? req.query.words.split(',') : false,

                    synsetids = req.query && req.query.synsetids ? req.query.synsetids.split(',') : false,


                    resXml = req.query && req.query.restype && req.query.restype.toLowerCase() === 'xml' ? true : false;

                var options = {fullData: true, statistics: true, semlinks: true, samples: true, recursive: true};

                getFullInMemWordData(words, synsetids, options, res, resXml);

            });


    }

}



var copyOptions = function (options) {
    var optionsOut = {};

    for (var x in options) {

        optionsOut[x] = options[x];

    }


    return optionsOut;

}

var respond = function (resData, responseObject, resXml) {

    var o2x = require('object-to-xml');


    resData.sort(function (a, b) {

        return a.word.lemma > b.word.lemma;

    });

    resData = titleByLemma(resData);

    if (!resXml) {

        responseObject.write(JSON.stringify(resData, null, 4));

        return responseObject.end();

    }

    responseObject.write(o2x({words: resData}));

    return responseObject.end();


}


function titleByLemma(wordArray) {

    var count = 0;

    var lastLemma = false;

    console.log("titleByLemma(wordArray): Array type:" + typeof(wordArray));

    if (wordArray instanceof Array) {
        console.log("Array type:array");
    }


    for (var x = 0; x < wordArray.length; x++) {


        if (lastLemma !== wordArray[x].word.lemma) {

            count = 0;

            lastLemma = wordArray[x].word.lemma

        }

        count += 1;

        wordArray[x].word.titleTag = wordArray[x].word.lemma + "(" + count + "):";

    }


    return wordArray;

}


var responseEval = function (optionsArr, resData, responseObject, resXml) {
    var optionsComplete = true;
    //turn the option off to track completion

    console.log(JSON.stringify(optionsArr, null, 4));

    for (var x = 0; x < optionsArr.length; x++) {

        for (var y in optionsArr[x]) {

            if (optionsArr[x][y]) {

                optionsComplete = false;

            }


        }


    }

    if (optionsComplete && !responseObject.finished) {

        respond(resData, responseObject, resXml);


    }


    return optionsComplete;

}


var getFullWordData = function (words, synsetids, options, responseObject, resXml) {

    if ((words.constructor !== Array && synsetids.constructor !== Array) || (words.constructor === Array && synsetids.constructor === Array)) {
        return false;
    }


    var wordDataOut = [];


    dexicon.WordWizard.getWords(words, synsetids, function (err, wordList) {

        if (!err) {

            var myOptions = [];

            for (var x = 0; x < wordList.length; x++) {

                myOptions[x] = copyOptions(options);

                //get the word statistics

                var wordRelationObj = wordList[x];

                wordDataOut.push({word: wordList[x]});


                if (myOptions[x].statistics) {


                    wordList[x].getStatistics(wordRelationObj, myOptions[x], function (err, data, options) {

                        if (err) {

                            return;
                        }

                        var str = JSON.stringify(data, null, 4);

                        console.log("Statistics:" + str);

                        //    wordRelationObj.statistics = data;


                        responseEval(myOptions, wordDataOut, responseObject, resXml);


                    }, function () {


                    });

                }


                //get the word semlinks


                if (myOptions[x].semlinks) {


                    if (myOptions[x].recursive && myOptions[x].recursiveLinkType && myOptions[x].recursiveDepth > 0) {

                        console.log(myOptions[x].recursiveLinkType);

                        wordList[x].getSemlinksRecursive(wordRelationObj, myOptions[x], function (err, data) {

                            if (err) {

                                return;
                                //code
                            }
                            var str = JSON.stringify(data, null, 4);

                            console.log("Semlinks:" + str);

                            //   wordRelationObj.semlinks = data;


                            responseEval(myOptions, wordDataOut, responseObject, resXml);


                        });

                    }
                    else {


                        wordList[x].getSemlinks(wordRelationObj, myOptions[x], function (err, data, options) {

                            if (err) {

                                return;
                                //code
                            }
                            var str = JSON.stringify(data, null, 4);

                            console.log("Semlinks:" + str);

                            //   wordRelationObj.semlinks = data;


                            responseEval(myOptions, wordDataOut, responseObject, resXml);


                        });


                    }


                }


                //get the word samples 

                if (myOptions[x].samples) {

                    wordList[x].getSamples(wordRelationObj, myOptions[x], function (err, data, options) {

                        if (err) {

                            return;
                            //code
                        }
                        var str = JSON.stringify(data, null, 4);

                        console.log("Samples:" + str);

                        //   wordRelationObj.samples = data;

                        responseEval(myOptions, wordDataOut, responseObject, resXml);


                    }, function () {



                        //    responseEval(myOptions, wordDataOut, responseObject);


                    });

                }


            }


            if (!options.statistics && !options.semlinks && !options.samples) {

                respond(wordDataOut, responseObject, resXml);


            }


        }


    });

}


var removeProps = function (obj) {


    delete obj['$loki'];

    delete obj['rowid'];

    return obj;

}


function containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
        if (list[i] === obj) {
            return true;
        }
    }

    return false;
}


var uniqueMembers = function (list) {

    var listOut = [];


    for (var x = 0; x < list.length; x++) {

        if (!containsObject(list[x], listOut)) {

            listOut.push(list[x]);

        }


    }


    return listOut;

}


var getDistinctSynsetIds = function (lexdomainname) {

    var sids = [];


}


var getFullInMemWordData = function (words, synsetids, options, responseObject, resXml) {

    var usingSynsets = words.constructor !== Array && synsetids.constructor == Array ? true : false;

    var wordDataOut = [];

    if ((words.constructor !== Array && synsetids.constructor !== Array)) {

        responseObject.end();

        return false;
    }


    if (!usingSynsets) {

        if (words.length > 10) {

            var response = {

                status: "invalid-request",

                message: "no more than 10 words per request"

            }

            responseObject.end(JSON.stringify(response, null, 4));

            return false;

        }

        words = uniqueMembers(words);


        wordDataOut = WordRetrieval.getWordsByLemmas(words);


    }
    else {

        if (synsetids.length > 10) {

            var response = {

                status: "invalid-request",

                message: "no more than 10 synsetids per request"

            }

            responseObject.end(JSON.stringify(response, null, 4));

            return false;

        }

        synsetids = uniqueMembers(synsetids);


        wordDataOut = WordRetrieval.getWordsBySynsetids(synsetids);

    }


    if (options.fullData || options.semlinks) {


        var allWordsLinks = inMemData.db.getCollection('semlinksspecial');

        var semlinksPerRecursiveLimit = 10;

        for (var x = 0; x < wordDataOut.length; x++) {

            var sid = parseInt(wordDataOut[x].word.synsetid);

            var data = WordRetrieval.getSemlinksBySynsetid(sid, options.recursive);

            wordDataOut[x].word.semlinks = [];

            for (var z = 0; z < data.length; z++) {

                wordDataOut[x].word.semlinks.push(data[z]);


            }


        }


    }


    if (options.fullData || options.samples) {


        for (var x = 0; x < wordDataOut.length; x++) {


            var word = wordDataOut[x].word;

            word.samples = WordRetrieval.getSamplesBySynsetid(word.wordid, word.synsetid);


        }

    }

    if (options.fullData || options.statistics) {

        for (var x = 0; x < wordDataOut.length; x++) {


            var word = wordDataOut[x].word;

            word.statistics = WordRetrieval.getStatisticsByWordidAndPos(word.wordid, word.pos);


        }


    }


    console.log(JSON.stringify(wordDataOut, null, 4));

    responseObject.write(JSON.stringify(wordDataOut, null, 4));

    responseObject.end();


}


var mainInit = function () {

    expressAndHTTPServerControls.httpGetInitMain();

}


var WordRetrieval = {

    allWords:false,

    allWordsLinks:false,

    allWordsSamples:false,

    allWordsStats:false,

    getWordsByLexdomainname: function (lexdomainname) {

        var wordDataOut = [];

        for (var x = 0; x < words.length; x++) {

           this.allWords = this.allWords || inMemData.db.getCollection('wordsspecial');

            var data = this.allWords.find({lexdomainname: lexdomainname});

            for (var y = 0; y < data.length; y++) {


                var wordObj = {word: removeProps(data[y])};

                if (wordDataOut.indexOf(wordObj) === -1 && wordObj.hasOwnProperty(lexdomainname)) {

                    wordDataOut.push(wordObj.lexdomainname);
                }


            }

        }

        return wordDataOut;

    },


    getWordsByLemmas: function (words) {

        var wordDataOut = [];

        for (var x = 0; x < words.length; x++) {

            this.allWords = this.allWords || inMemData.db.getCollection('wordsspecial');

            var data = this.allWords.find({lemma: words[x]});

            for (var y = 0; y < data.length; y++) {

                if(data[y].hasOwnProperty('lemma')) {

                    data[y].titleTag = data[y].lemma;

                }

                    var wordObj = {word: removeProps(data[y])};

                    if (wordDataOut.indexOf(wordObj) === -1) {

                        wordDataOut.push(wordObj);
                    }



            }

        }

        return wordDataOut;

    },


    getWordsBySynsetids: function (synsetids) {


        var wordDataOut = [];

        for (var x = 0; x < synsetids.length; x++) {

            this.allWords = this.allWords ||inMemData.db.getCollection('wordsspecial');

            var data = this.allWords.find({synsetid: synsetids[x]});

            for (var y = 0; y < data.length; y++) {

                data[y].titleTag = data[x].lemma;

                var wordObj = {word: removeProps(data[y])};

                if (wordDataOut.indexOf(wordObj) === -1) {

                    wordDataOut.push(wordObj);
                }


            }

        }


        return wordDataOut;

    },


    getSemlinksBySynsetid: function (synsetid, recursive) {

        var semlinks = [];

        this.allWordsLinks = this.allWordsLinks ||inMemData.db.getCollection('semlinksspecial');

        var semlinksPerRecursiveLimit = 10;

        var sid = parseInt(synsetid);

        this.allWords = this.allWords || inMemData.db.getCollection('wordsspecial');


        var _inst = this;

        var linkedWords = function(sid)
        {

            var obj = _inst.allWordsLinks.find({synset1id: sid});

            return obj;


        };


        var data = linkedWords(sid);


        for (var z = 0; z < data.length; z++) {

            var obj = removeProps(data[z]);

            semlinks.push(data[z]);


        }


        if (recursive) {

            //get the UPTRAIL recursive semlinks IF totalWords < 20 , maxRecursive = 5; maxRecursiveRowPerResult: 50;

            for (var y = 0; y < semlinks.length; y++) {

                var word = semlinks[y];

                if (word.link.indexOf('hypernym') > -1 || word.link.indexOf('meronym') > -1) {

                    var link = word.link, sid = word.synset2id;


                    var data = linkedWords(sid);


                    if (data.length > 0) {


                        for (var z = 0; z < data.length; z++) {

                            //  console.log(data[z].link);

                            var obj = removeProps(data[z]);


                            if (obj.link === link && semlinks.indexOf(obj) === -1) {

                                semlinks.push(obj);

                                //code
                            }

                        }

                        //code
                    }


                }

            }


        }


        return semlinks;

    }

    ,

    getSamplesBySynsetid: function (synsetid, wordid) {

        var samples = [];

      this.allWordsSamples = this.allWordsSamples || inMemData.db.getCollection('samplesspecial');

        var data = [];

        if (wordid) {
            data = this.allWordsSamples.find({wordid: wordid, synsetid: synsetid});
        }
        else {
            data = this.allWordsSamples.find({wordid: wordid, synsetid: synsetid});
        }


        for (var z = 0; z < data.length; z++) {


            var obj = removeProps(data[z]);


            samples.push(obj);

        }


        return samples;

    }
    ,


    getStatisticsByWordidAndPos: function (wordid, pos) {

      this.allWordsStats = this.allWordsStats || inMemData.db.getCollection('statisticsspecial');


        var data = this.allWordsStats.find({wordid: wordid, pos: pos});

        var statistics = [];

        for (var z = 0; z < data.length; z++) {


            var obj = removeProps(data[z]);

            statistics.push(obj);

        }


        return statistics;

    }


}


mainInit();


server.listen(port);

console.log('Allow several seconds to start. Server running at http://' + port + '/');


inMemData.db = dexicon.sqliteToInMemDb(dexiconData, ['wordsspecial', 'semlinksspecial', 'samplesspecial', 'statisticsspecial']).db;


return{

    WordRetrieval:WordRetrieval,

    app:app

}

};




