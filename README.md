Dexicon
=======

A powerful solution for lexicon-data
------------------------------------

### Welcome to the repository ###

* Dexicon is a fast, easy-to-use module for retrieval of lexicon-data. Includes simple Rest API for javascript clients.
  :Access to words, definitions, synonyms, antonyms, and related data
  :Relational data on linked-words(wn-semlinks), such as *hypernyms*, *hyponyms*, *holonyms*, and *meronyms*.

  Feel free to access our [live dexicon demo](http://dexicon.com/)

  Reasons to use dexicon:

  1.Speed:an in-mem lexicon means fast results with extensive data
  2.Convenience:works right out of the box, with documented examples
  3.Versatility:use dexicon on server side, or access the rest-api with a javascript client

  Heads up about size and memory:

  1. heavy package of several-hundred MB or more.
  2. uses mucho ram (the same) in running an in-memory lexicon
  3. the module will download 97MB database from bitbucket.com on first install

Our goal with dexicon is to provide an easy-to-use kit for access of word-related-data.

# Contributions #

[![Donate](https://img.shields.io/badge/Donate-PayPal-green.svg)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=M9ZQEJM7T2XLW)


* Version 1.1.4

### How do I get set up? ###

* nodejs

## Preferred Setup: ##

 download [our bitbucket project](https://bitbucket.org/slick-resources/node-dexicon) and apply the app.js file directly.

## Alternate Setup: ##

* npm install node-dexicon

* copy / paste the sqlite wordwiz.db file from [our bitbucket project](https://bitbucket.org/slick-resources/node-dexicon)
        -put this in the root directory of your project, aka replace the 0byte file from npm with the full database (several 100 MB).
        -npm start the module and node-dexicon should then initialize.
* Database:module uses lokijs database, assembled at runtime from a the wordwiz.db (a modified Wordnet)
* note: loki: in-mem database requires high ram/runtime-memory of ~1GB

### Module usage ###

    * Instantiate the dexicon module:

        var dexicon_module = require('node-dexicon')(8082 /*port number*/); //default port is 80

        //module will take several seconds to instantiate an in-mem lexicon, requires mucho ram on node

### Rest-Api usage: word request examples ###


    * request all data for words: cat and dog

        $.getJSON('<#app_url>/dexicon/api/deep?words=cat,dog', function(data){

         console.log("Got json data:" + JSON.stringify(data));

        });

    * request statistics data for synsetids from existing word request

        $.getJSON('<#app_url>/dexicon/api/statistics?synsetids=<id-value-1>,<id-value-2>', function(data){

        console.log("Got json data:" + JSON.stringify(data));

        });


### rest-Api:uris: ###

    /dexicon/api/basic  : get only the basic word-data

    /dexicon/api/deep   : deep response of all data pertaining to a word- This option will retrieve deep results

    /*searching the word 'lama' brings results for lama >> mammal >> quadriped >> animal >> animate-being */

    /dexicon/api/statistics : get bnc word-usage statistics for words

    /dexicon/api/samples : samples of word-usage

    /dexicon/api/semlinks : get semlinks of a word, such as hypernyms, hyponyms

### Api:parameters ###

    'words', as in 'http://my_url?words=word1,word2,word3'

    'synsetids', as in 'http://my_url?synsetids=s1,s2,s3' //synsetids would usually come from an existing data request

### Who is the owner/creator of project? ###

    * Jordan E. Blake  :: jordan9991080@gmail.com