
var https = require('https');
var fs = require('fs');

var fullWordData = [];

var addr = 'https://bitbucket.org/trickmysys/node-dexicon/raw/HEAD/wordwiz.db',

//file = fs.createWriteStream(__dirname + "/wordwiz.db");

    callback = function(response) {

        console.log('Obtaining lexicon dependencies from https://bitbucket.org/trickmysys/node-dexicon/');

        response.on('data', function (chunk) {

            fullWordData.push(chunk);

        });

        response.on('end', function () {


            var fd =  fs.openSync(__dirname + "/wordwiz.db", 'w');

            var buffer = Buffer.concat(fullWordData);

            fs.write(fd, buffer, 0, buffer.length, 0, function(err,written){

                console.log('Data file complete');

            });


            console.log("lexicon download complete.");
        });

    };


https.get(addr, callback);
