/*
By Jordan Blake
-search module for a modified wordnet lexicon in sqlite
*/
module.exports = {
     
    

        
        getSemlinksByType:function(sqliteDb, options, synset1id, linkType, myArray, recursiveDepth, callback)
        {
          

                       sqliteDb.all("SELECT *, definition AS synset2Definition FROM semlinksspecial, words WHERE synset1id = '"+synset1id+"' AND link = '"+linkType+"' AND words.synsetid = semlinksspecial.synset2id  LIMIT 0,1", function(err, rows) {

                              if (err) {
                                        return callback(err, myArray); //code
                              }

                             if (!rows.length > 0) {
                                console.log("Zero Results:");
                                
                                options.recursiveDepth = false;
                                
                                options.recursive = false;
                                
                                options.recursiveLinkType = false;
                                
                                options.semlinks = false;
                                
                                
                                  return callback(false, myArray); //code
                                
                             }
                           
                              
                              for(var x = 0; x < rows.length; x++)
                              {
                                
                                //  console.log("Data");
                                
                                
                                if (!myArray.indexOf(rows[x]) >= 0) {
                                     myArray.push(rows[x]);
                                }
                                
                            
                              
                             // tables.semlinks.insert(rows[x]);
                             
                                 if (recursiveDepth > -1) {
                                   
                                   console.log('Decreased Depth to:' + recursiveDepth);
                                
                                recursiveDepth -= 1;
                                
                               
                           module.exports.getSemlinksByType(sqliteDb, options, rows[x].synset2id, rows[x].link, myArray, recursiveDepth, callback);
                                
                                
                             }
                             else
                             {
                                
                                   
                                options.recursiveDepth = false;
                                
                                options.recursive = false;
                                
                                options.recursiveLinkType = false;
                                
                                  
                                options.semlinks = false;
                                
                                
                                return callback(false, myArray); //code
                                
                             }
                            
                              }
                              
                              
                       });
                
                
                return myArray;
                
        },

          init: function(sqliteDb) {
                    
                      
                  var  loki = require('lokijs');
          

                  var  db = new loki('loki4.json');

                  var  tables = this.tables;
                  
                  
                  var db = new loki('test.json');
                        db.loadDatabase({}, function() {
                

                    tables.words = db.addCollection('words');
                     tables.semlinks = db.addCollection('semlinks');
                      tables.samples = db.addCollection('samples');
                       tables.statistics = db.addCollection('statistics');
                
                module.exports.loadData(sqliteDb, tables);
                
                 
                });
                    
                 
                       
                  
                       
                    /*
                       
                          sqliteDb.each("SELECT * FROM samples", function(err, row) {

                              if (err) {
                                        return console.log(err); //code
                              }

                             
                              console.log("Data");
                              
                              tables.samples.insert(row);
                              
                    });
                          
                          
                             sqliteDb.each("SELECT * FROM statistics", function(err, row) {

                              if (err) {
                                        return console.log(err); //code
                              }

                             
                              console.log("Data");
                              
                              tables.statistics.insert(row);
                              
                    });
                             
                          */   
                 
                    

          },
          
          
            getQueryInStr: function(listIn) {


  console.log("getQueryInStr():" + listIn);

                    var strOut = "(";

                    for (var x = 0; x < listIn.length; x++) {

                              console.log("getQueryInStr():" + listIn[x]);


                              if (x > 0) {

                                        strOut += ",";
                              }


                              strOut += "'" + listIn[x].trim() + "' ";


                    }

                    strOut += ")";

                    return strOut;

          }


          
}



var sqlite3 = require("sqlite3").verbose();

var fs = require("fs");

var file = "wordwiz.db";
var exists = fs.existsSync(file);

if (exists) {
       var lexData = new sqlite3.Database(file);
       
       var linkTypes = ['hypernym','hyponym'];
   
     // module.exports.getSemlinksByType(lexData, '101677105', linkTypes, [], 10, function(links){
        
     //   console.log(JSON.stringify(links));

   //   });
     
          
                    
     
       
}





